package com.pippin.design.behavioral.templatemethod;

/**
 * 模板方法抽象类，算法骨架。
 * 将复杂的操作步骤抽出到一个类中。
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public abstract class ACourse {

    protected final void makeCourse(){
        this.makePPT();
        this.makeVideo();
        if(needWriteArticle()){
            this.writeArticle();
        }
        this.packageCourse();
    }

    final void makePPT(){
        System.out.println("制作PPT");
    }
    final void makeVideo(){
        System.out.println("制作视频");
    }
    final void writeArticle(){
        System.out.println("编写手记");
    }

    //钩子方法
    protected boolean needWriteArticle(){
        return false;
    }

    // 由子类为一个或多个步骤提供实现
    abstract void packageCourse();

}
