package com.pippin.design.behavioral.templatemethod;

/**
 * 模板方法实现类。
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class DesignPatternCourse extends ACourse {
    @Override
    void packageCourse() {
        System.out.println("提供课程Java源代码");
    }

    @Override
    protected boolean needWriteArticle() {
        return true;
    }

}
