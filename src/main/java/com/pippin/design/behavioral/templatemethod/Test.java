package com.pippin.design.behavioral.templatemethod;

/**
 * 模板方法模式
 * 定义：定义了一个算法的骨架，并允许子类为一个或多个步骤提供实现。
 * 模板方法使得子类可以在不改变算法结构的情况下，重新定义算法的某些步骤。
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class Test {
    public static void main(String[] args) {
//        System.out.println("后端设计模式课程start---");
//        ACourse designPatternCourse = new DesignPatternCourse();
//        designPatternCourse.makeCourse();
//        System.out.println("后端设计模式课程end---");


        System.out.println("前端课程start---");
        ACourse feCourse = new FECourse(false);
        feCourse.makeCourse();
        System.out.println("前端课程end---");


    }
}
