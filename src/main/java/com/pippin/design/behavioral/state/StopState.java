package com.pippin.design.behavioral.state;

/**
 * 状态实现类（停止状态）
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class StopState extends CourseVideoState {
    @Override
    public void play() {
        super.courseVideoContext.setCourseVideoState(CourseVideoContext.PLAY_STATE);
    }

    @Override
    public void speed() {
        System.out.println("Error 停止状态不能快进");
    }

    @Override
    public void pause() {
        System.out.println("Error 停止状态不能暂停");
    }

    @Override
    public void stop() {
        System.out.println("停止播放课程视频状态");
    }
}
