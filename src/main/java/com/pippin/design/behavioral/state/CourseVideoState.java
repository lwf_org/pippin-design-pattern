package com.pippin.design.behavioral.state;

/**
 * 定义状态抽象类，定义几种状态接口
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public abstract class CourseVideoState {

    protected CourseVideoContext courseVideoContext;

    public void setCourseVideoContext(CourseVideoContext courseVideoContext) {
        this.courseVideoContext = courseVideoContext;
    }

    //正常播放
    public abstract void play();

    //加速播放
    public abstract void speed();

    //暂停播放
    public abstract void pause();

    //停止播放
    public abstract void stop();

}
