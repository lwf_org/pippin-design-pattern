package com.pippin.design.behavioral.state;

/**
 * 课程视频上下文
 * 通过setCourseVideoContext（）方法，这里会出现循环引用，但这不影响内存，因为引用对象是同一个。
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class CourseVideoContext {

    private CourseVideoState courseVideoState;
    public final static PlayState PLAY_STATE = new PlayState();
    public final static StopState STOP_STATE = new StopState();
    public final static PauseState PAUSE_STATE = new PauseState();
    public final static SpeedState SPEED_STATE = new SpeedState();

    public CourseVideoState getCourseVideoState(){
        return courseVideoState;
    }

    public void setCourseVideoState(CourseVideoState courseVideoState) {
        this.courseVideoState = courseVideoState;
        this.courseVideoState.setCourseVideoContext(this);
    }

    public void play(){
        this.courseVideoState.play();
    }

    public void stop(){
        this.courseVideoState.stop();
    }

    public void pause(){
        this.courseVideoState.pause();
    }

    public void speed(){
        this.courseVideoState.speed();
    }
}
