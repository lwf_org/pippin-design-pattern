package com.pippin.design.behavioral.chainofresponsibility;

import org.apache.commons.lang3.StringUtils;

/**
 * 审批链实现类
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class VideoApprover extends Approver {
    @Override
    public void deploy(Course course) {
        if(StringUtils.isNotBlank(course.getVideo())){
            System.out.println(course.getName() + "含有视频，批准");
            if(approver != null){
                approver.deploy(course);
            }
        }else{
            System.out.println(course.getName() +"不含视频，不批准");
        }
    }
}
