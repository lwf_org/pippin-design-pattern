package com.pippin.design.behavioral.chainofresponsibility;

/**
 * 责任链模式
 * 定义：为请求创建一个接受此次请求对象的链。
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class Test {

    public static void main(String[] args) {
        Approver articleApprover = new ArticleApprover();
        Approver videoApprover = new VideoApprover();

        Course course = new Course();
        course.setName("java设计模式");
        course.setArticle("java设计模式手记");
        course.setVideo("java设计模式视频");

        articleApprover.setNextApprover(videoApprover);
        articleApprover.deploy(course);
    }
}
