package com.pippin.design.behavioral.chainofresponsibility;

import org.apache.commons.lang3.StringUtils;

/**
 * 审批链实现类
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class ArticleApprover extends Approver {
    @Override
    public void deploy(Course course) {
        if(StringUtils.isNotBlank(course.getArticle())){
            System.out.println(course.getName() + "含有手记，批准");
            if(approver != null){
                approver.deploy(course);
            }
        }else{
            System.out.println(course.getName() +"不含手记，不批准");
        }
    }
}
