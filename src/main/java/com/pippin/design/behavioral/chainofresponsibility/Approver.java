package com.pippin.design.behavioral.chainofresponsibility;

/**
 * 审批链抽象类
 * 通过setNextApprover（）方法将形成一条链条。
 * 代码格式：责任链模式对应的批准者要包含一个自己类型的批准者。
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public abstract class Approver {
    protected Approver approver;

    public void setNextApprover(Approver approver){
        this.approver = approver;
    }

    public abstract void deploy(Course course);

}
