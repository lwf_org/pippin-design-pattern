package com.pippin.design.behavioral.mediator;

/**
 * 中介者模式
 * 定义：定义一个封装一组对象如何交互的对象。
 * 通过使对象明确地相互引用来促进松散耦合，并允许独立地改变它们的交互。
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class Test {

    public static void main(String[] args) {
        User tony = new User("Tony");
        User natus = new User("Natus");

        natus.sendMessage("Tony，Give me a haircut");
        tony.sendMessage("OK! Natus");
    }
}
