package com.pippin.design.behavioral.mediator;

import java.util.Date;

/**
 * 中介者模式
 * 这里定义了发送消息的固定模板，也就是交互的方式。
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class StudyGroup {

    public static void showMessage(User user, String message){
        System.out.println(new Date().toString() + "[" + user.getName() +"]:" + message);
    }
}
