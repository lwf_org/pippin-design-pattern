package com.pippin.design.behavioral.mediator;

/**
 * 用户实体类
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class User {
    private String name;

    public User(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    // 调用发送消息方法
    public void sendMessage(String messgae){
        StudyGroup.showMessage(this, messgae);
    }
}
