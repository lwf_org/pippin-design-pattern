package com.pippin.design.behavioral.interpreter;

/**
 * 操作类接口
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public interface Interpreter {

    int interpret();
}
