package com.pippin.design.behavioral.interpreter;

/**
 * 解释器模式
 * 定义：给定一个语言，定义它的文法的一种表示，并定义一个解释器，这个解释器使用该表示来解释语言中的句子。
 * 为了解释一种语言，而为语言创建的解释器。
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class Test {

    public static void main(String[] args) {
        String inputStr = "6 100 11 + *";
        ExpressionParser expressionParser = new ExpressionParser();
        int result = expressionParser.parse(inputStr);
        System.out.println("解释器计算结果：" + result);
    }
}
