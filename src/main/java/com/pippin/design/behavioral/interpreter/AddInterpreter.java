package com.pippin.design.behavioral.interpreter;

/**
 * 运算操作类（加法）
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class AddInterpreter implements Interpreter{
    private Interpreter firstExpression, secondExpression;

    public AddInterpreter(Interpreter firstExpression, Interpreter secondExpression){
        this.firstExpression = firstExpression;
        this.secondExpression = secondExpression;
    }

    @Override
    public String toString() {
        return "+";
    }

    @Override
    public int interpret() {
        return this.firstExpression.interpret() + this.secondExpression.interpret() ;
    }
}
