package com.pippin.design.behavioral.interpreter;

import java.util.Stack;

/**
 * 自定义的解释器，这里解释了运算符和数值的关系
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class ExpressionParser {

    private Stack<Interpreter> stack = new Stack<>();

    public int parse(String str){
        String[] split = str.split(" ");
        for (String symbol : split) {
            if (!OperatorUtil.isOperator(symbol)){
                Interpreter numberExpression = new NumberInterpreter(symbol);
                stack.push(numberExpression);
                System.out.println(String.format("入栈：%d", numberExpression.interpret()));
            }else{
                //是运算符号，可以计算
                Interpreter firstExpression = stack.pop();
                Interpreter secondExpression = stack.pop();
                System.out.println(String.format("出栈：%d 和 %d", firstExpression.interpret(), secondExpression.interpret()));
                Interpreter Operator = OperatorUtil.getExpressionObject(firstExpression, secondExpression, symbol);
                System.out.println(String.format("运用运算符 %s", symbol));
                int interpret = Operator.interpret();
                NumberInterpreter numberInterpreter = new NumberInterpreter(interpret);
                stack.push(numberInterpreter);
                System.out.println(String.format("阶段结果入栈：%d", numberInterpreter.interpret()));
            }
        }
        return stack.pop().interpret();
    }

}
