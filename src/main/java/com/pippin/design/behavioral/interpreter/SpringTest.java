package com.pippin.design.behavioral.interpreter;

import org.springframework.expression.Expression;
import org.springframework.expression.spel.standard.SpelExpressionParser;

/**
 * spring中的解释器
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class SpringTest {

    public static void main(String[] args) {
        org.springframework.expression.ExpressionParser parser = new SpelExpressionParser();
        Expression expression = parser.parseExpression("100 * 2 + 400 * 1 + 66");
        int value = (Integer) expression.getValue();
        System.out.println(value);
    }
}
