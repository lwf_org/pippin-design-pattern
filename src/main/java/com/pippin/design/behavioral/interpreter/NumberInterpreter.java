package com.pippin.design.behavioral.interpreter;

/**
 * 数值操作类
 * 这里仅仅是做了转换类型
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class NumberInterpreter implements Interpreter{
    private int number;

    public NumberInterpreter(int number){
        this.number = number;
    }
    public NumberInterpreter(String number){
        this.number = Integer.valueOf(number);
    }

    @Override
    public int interpret() {
        return this.number;
    }
}
