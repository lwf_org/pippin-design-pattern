package com.pippin.design.behavioral.interpreter;

/**
 * 工具类
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class OperatorUtil {

    //判断是否为运算符
    public static boolean isOperator(String symbol){
        return (symbol.equals("+") || symbol.equals("*"));
    }

    //根据运算符返回运算操作类
    public static Interpreter getExpressionObject(Interpreter firstExpression, Interpreter secondExpression, String symbol){
        if (symbol.equals("+")){
            return new AddInterpreter(firstExpression, secondExpression);
        }else if(symbol.equals("*")){
            return new MultiInterpreter(firstExpression, secondExpression);
        }else {
            return null;
        }
    }
}
