package com.pippin.design.behavioral.memento;

import java.util.Stack;

/**
 * 存储手记快照
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class ArticleMementoManager {

    private final Stack<ArticleMemento> ARTICLE_MEMENTO_STACK = new Stack<>();

    public ArticleMemento getMemento(){
        return ARTICLE_MEMENTO_STACK.pop();
    }

    public void addMemento(ArticleMemento articleMemento){
        ARTICLE_MEMENTO_STACK.push(articleMemento);
    }
}
