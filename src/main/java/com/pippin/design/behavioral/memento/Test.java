package com.pippin.design.behavioral.memento;

/**
 * 备忘录模式
 * 定义：保存一个对象的某个状态，以便在适当的时候恢复对象。
 * 后悔药
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class Test {

    public static void main(String[] args) {
        ArticleMementoManager articleMementoManager = new ArticleMementoManager();

        Article article = new Article("备忘录模式手记A", "手记内容A", "手记图片A");
        ArticleMemento articleMemento = article.saveToMemento();
        articleMementoManager.addMemento(articleMemento);
        System.out.println("标题：" + article.getTitle() + "内容：" + article.getContent() + "图片：" + article.getImgs());

        System.out.println("修改手记StartB");
        article.setTitle("备忘录模式手记B");
        article.setContent("手记内容B");
        article.setImgs("手记图片B");
        System.out.println("修改手记EndB");
        System.out.println("手记完整信息" + article.toString());

        System.out.println("暂存回退Start");
        ArticleMemento articleMementoB = article.saveToMemento();
        articleMementoManager.addMemento(articleMementoB);

        System.out.println("修改手记StartC");
        article.setTitle("备忘录模式手记C");
        article.setContent("手记内容C");
        article.setImgs("手记图片C");
        System.out.println("修改手记EndC");
        System.out.println("手记完整信息" + article.toString());

        System.out.println("回退出栈一次");
        articleMemento = articleMementoManager.getMemento();
        article.undoFromMemento(articleMemento);

        System.out.println("回退出栈两次");
        articleMemento = articleMementoManager.getMemento();
        article.undoFromMemento(articleMemento);

        System.out.println("暂存回退end");
        System.out.println("手记完整信息"+ article.toString());
    }
}
