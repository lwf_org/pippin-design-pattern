package com.pippin.design.behavioral.observer;

/**
 * 观察者模式
 * 定义：定义了对象之间的一对多依赖，让多个观察者对象同时监听某一个主题对象，
 * 当主题对象发生变化时，它的所有依赖着（观察者）都会受到通知并更新。
 * jdk中有提供实现观察者模式的接口
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class Test {
    public static void main(String[] args) {
        Course course = new Course("java设计模式");
        Teacher teacher = new Teacher("Tony");
        course.addObserver(teacher);

        //业务逻辑代码
        Question question = new Question();
        question.setUserName("pippin");
        question.setQuestionContent("观察者模式如何写");

        course.produceQuestion(course, question);
    }
}
