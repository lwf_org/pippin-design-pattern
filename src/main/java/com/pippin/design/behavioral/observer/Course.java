package com.pippin.design.behavioral.observer;

import java.util.Observable;

/**
 * 被观察者
 * 继承Observable，并调用setChanged()方法和notifyObservers()方法
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class Course extends Observable{

    private String courseName;

    public Course(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseName() {
        return courseName;
    }

    public void produceQuestion(Course course, Question question){
        System.out.println(question.getUserName() + "在" + course.getCourseName() +"提交了一个问题");
        //通知观察者这里有做修改了。
        setChanged();
        //传递修改数据
        notifyObservers(question);
    }
}
