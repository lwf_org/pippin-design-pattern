package com.pippin.design.behavioral.observer;

/**
 * 传递数据类
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class Question {

    private String userName;

     private String questionContent;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getQuestionContent() {
        return questionContent;
    }

    public void setQuestionContent(String questionContent) {
        this.questionContent = questionContent;
    }
}
