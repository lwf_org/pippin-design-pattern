package com.pippin.design.behavioral.strategy;

/**
 * 策略类
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class ManJianPromotionStrategy implements PromotionStrategy{
    @Override
    public void doPromotion() {
        System.out.println("满减促销,满200-20元");
    }
}
