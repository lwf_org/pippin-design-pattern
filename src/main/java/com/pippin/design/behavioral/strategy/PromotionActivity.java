package com.pippin.design.behavioral.strategy;

/**
 * 使用策略类的类
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class PromotionActivity {
    private PromotionStrategy promotionStrategy;

    public PromotionActivity(PromotionStrategy promotionStrategy) {
        this.promotionStrategy = promotionStrategy;
    }

    public void executePromotionStrategy(){
        promotionStrategy.doPromotion();
    }

}
