package com.pippin.design.behavioral.strategy;

/**
 * 定义策略接口
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public interface PromotionStrategy {

    //定义实现方法
    void doPromotion();
}
