package com.pippin.design.behavioral.strategy;

/**
 * 策略类
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class FanXianPromotionStrategy implements PromotionStrategy{

    @Override
    public void doPromotion() {
        System.out.println("返现促销,返回的金额存放到慕课网用户的余额中");
    }
}
