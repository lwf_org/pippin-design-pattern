package com.pippin.design.behavioral.strategy;

/**
 * 策略类
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class LiJianPromotionStrategy implements PromotionStrategy {
    @Override
    public void doPromotion() {
        System.out.println("立减促销,课程的价格直接减去配置的价格");
    }
}
