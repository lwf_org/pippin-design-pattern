package com.pippin.design.behavioral.visitor;

/**
 * 课程抽象类
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public abstract class Course {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract void accept(IVisitor iVisitor);
}
