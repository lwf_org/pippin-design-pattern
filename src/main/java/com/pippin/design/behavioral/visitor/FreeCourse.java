package com.pippin.design.behavioral.visitor;

/**
 * （免费）课程实现类
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class FreeCourse extends Course  {

    @Override
    public void accept(IVisitor visitor) {
        visitor.visit(this);
    }
}
