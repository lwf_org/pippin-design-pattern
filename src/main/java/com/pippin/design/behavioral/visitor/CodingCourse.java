package com.pippin.design.behavioral.visitor;

/**
 * （实战）课程实现类
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class CodingCourse extends Course {

    private int price;

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public void accept(IVisitor visitor) {
        visitor.visit(this);
    }
}
