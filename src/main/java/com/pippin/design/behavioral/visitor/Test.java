package com.pippin.design.behavioral.visitor;

import java.util.ArrayList;
import java.util.List;

/**
 * 访问者模式
 * 定义：封装作用于某数据结构（如List、Set、Map等）中的个元素的操作。
 * 可以在不改变各元素的前提下，定义作用于这些元素的操作。
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class Test {

    public static void main(String[] args) {
        List<Course> courseList = new ArrayList<>();

        FreeCourse freeCourse = new FreeCourse();
        freeCourse.setName("java基础");

        CodingCourse codingCourse = new CodingCourse();
        codingCourse.setName("java设计模式");
        codingCourse.setPrice(300);
        courseList.add(freeCourse);
        courseList.add(codingCourse);

        for (Course course : courseList) {
            course.accept(new Visitor());
        }
    }
}
