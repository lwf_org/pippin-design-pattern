package com.pippin.design.behavioral.visitor;

/**
 * 可扩展其他Visitor
 * 访问者模式核心：根据不同的Visitor产生不同的操作行为
 *
 * @author : Natus
 * @date : 2020/8/27 0027 */
public class Visitor implements IVisitor {

    @Override
    public void visit(FreeCourse freeCourse) {
        System.out.println("免费课程："+freeCourse.getName());
    }

    @Override
    public void visit(CodingCourse codingCourse) {
        System.out.println("实战课程："+codingCourse.getName() + " 价格：" + codingCourse.getPrice());
    }
}
