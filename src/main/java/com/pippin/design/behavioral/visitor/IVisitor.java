package com.pippin.design.behavioral.visitor;

/**
 * 访问者接口
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public interface IVisitor {

    void visit(FreeCourse freeCourse);

    void visit(CodingCourse codingCourse);

}
