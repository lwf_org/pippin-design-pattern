package com.pippin.design.behavioral.iterator;

/**
 * 实例类
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class Course {
    private String name;

    public Course(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
