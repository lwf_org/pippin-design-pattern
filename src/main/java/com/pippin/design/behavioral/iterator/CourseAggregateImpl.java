package com.pippin.design.behavioral.iterator;

import java.util.ArrayList;
import java.util.List;

/**
 * 集合接口实现类
 * 通常我们迭代集合是通过for循环操作,但是通过反编译之后看到的代码就是通过迭代器来遍历集合的。
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class CourseAggregateImpl implements CourseAggregate {

    private List courseList;

    public CourseAggregateImpl() {
        this.courseList = new ArrayList();
    }

    @Override
    public void addCourse(Course course) {
        courseList.add(course);
    }

    @Override
    public void removeCourse(Course course) {
        courseList.remove(course);
    }

    @Override
    public CourseIterator getCourseIterator() {
        return new CourseIteratorImpl(courseList);
    }
}
