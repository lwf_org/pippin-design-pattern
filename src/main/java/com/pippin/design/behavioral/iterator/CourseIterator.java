package com.pippin.design.behavioral.iterator;

/**
 * 迭代器模式
 * 迭代器接口
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public interface CourseIterator {

    //定义下一元素接口
    Course nextCourse();

    //定义是否是最后一个元素
    boolean isLastCourse();

}
