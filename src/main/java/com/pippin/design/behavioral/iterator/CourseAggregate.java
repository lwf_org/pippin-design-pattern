package com.pippin.design.behavioral.iterator;

/**
 * 集合接口
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public interface CourseAggregate {

    //添加元素
    void addCourse(Course course);

    //删除元素
    void removeCourse(Course course);

    //获取迭代器
    CourseIterator getCourseIterator();



}
