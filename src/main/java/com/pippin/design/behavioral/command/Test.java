package com.pippin.design.behavioral.command;

/**
 * 命令模式
 * 定义：将“请求”封装成对象，以便使用不同的请求。
 * 命令模式解决了应用程序中对象的职责以及它们之间的通信方式。
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class Test {

    public static void main(String[] args) {
        CourseVideo courseVideo = new CourseVideo("命令模式");
        OpenCourseVideoCommand openCourseVideoCommand = new OpenCourseVideoCommand(courseVideo);
        CloseCourseVideoCommand closeCourseVideoCommand = new CloseCourseVideoCommand(courseVideo);

        Staff staff = new Staff();
        staff.addCommand(openCourseVideoCommand);
        staff.addCommand(closeCourseVideoCommand);

        staff.executeCommand();
    }
}
