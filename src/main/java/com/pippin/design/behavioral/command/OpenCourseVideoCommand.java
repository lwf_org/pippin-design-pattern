package com.pippin.design.behavioral.command;

/**
 * 命令接口实现类
 * 开放课程命令
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class OpenCourseVideoCommand implements Command {

    private CourseVideo courseVideo;

    public OpenCourseVideoCommand(CourseVideo courseVideo) {
        this.courseVideo = courseVideo;
    }

    @Override
    public void execute() {
        courseVideo.open();
    }
}
