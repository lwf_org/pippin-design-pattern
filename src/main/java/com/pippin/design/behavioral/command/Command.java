package com.pippin.design.behavioral.command;

/**
 * 命令接口类
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public interface Command {

    void execute();
}
