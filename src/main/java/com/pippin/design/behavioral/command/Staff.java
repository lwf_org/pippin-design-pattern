package com.pippin.design.behavioral.command;

import java.util.ArrayList;
import java.util.List;

/**
 * 员工类
 * 执行命令集合
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class Staff {

    private List<Command> commandList = new ArrayList<>();

    public void addCommand(Command command){
        commandList.add(command);
    }

    public void executeCommand(){
        for (Command command : commandList) {
            command.execute();
        }
        commandList.clear();
    }
}
