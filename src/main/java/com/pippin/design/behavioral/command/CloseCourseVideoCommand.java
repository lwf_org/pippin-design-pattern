package com.pippin.design.behavioral.command;

/**
 * 命令接口实现类
 * 关闭课程命令
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class CloseCourseVideoCommand implements Command {

    private CourseVideo courseVideo;

    public CloseCourseVideoCommand(CourseVideo courseVideo) {
        this.courseVideo = courseVideo;
    }

    @Override
    public void execute() {
        courseVideo.close();
    }
}
