package com.pippin.design.behavioral.command;

/**
 * 课程视频类
 * 将其方法抽象成类，单独执行。
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class CourseVideo {

    private String name;

    public CourseVideo(String name) {
        this.name = name;
    }

    public void open(){
        System.out.println(this.name + "视频课程开放");
    }

    public void close(){
        System.out.println(this.name + "视频课程关闭");
    }
}
