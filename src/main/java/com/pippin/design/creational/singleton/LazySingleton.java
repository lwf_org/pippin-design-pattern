package com.pippin.design.creational.singleton;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

/**
 * 单例模式
 * 通过在调用getInstance()方法时，创建单例对象实例懒加载
 * 因为通过反射可以调用构造器生成新的实例对象，所以在私有构造器中判断非null抛出异常
 * 通过synchronized实例线程安全，全局只产生一个实例类
 *
 * 缺点：使用synchronized修饰静态方法,实例类的锁定。效率低下，同一时间只有一个线程能访问getInstance（）。
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class LazySingleton {
    private static LazySingleton lazySingleton = null;
    private LazySingleton(){
        if(lazySingleton != null){
            throw new RuntimeException("单例构造器禁止反射调用");
        }
    }
    public synchronized static LazySingleton getInstance(){
        if(lazySingleton == null){
            lazySingleton = new LazySingleton();
        }
        return lazySingleton;
    }

    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchFieldException {
        Class objectClass = LazyDoubleCheckSingleton.class;
        Constructor c = objectClass.getDeclaredConstructor();
        c.setAccessible(true);

        LazyDoubleCheckSingleton o1 = LazyDoubleCheckSingleton.getInstance();

        Field flag = o1.getClass().getDeclaredField("flag");
        flag.setAccessible(true);
        flag.set(o1,true);

        LazyDoubleCheckSingleton o2 = (LazyDoubleCheckSingleton) c.newInstance();

        System.out.println(o1);
        System.out.println(o2);
        System.out.println(o1==o2);
    }

}
