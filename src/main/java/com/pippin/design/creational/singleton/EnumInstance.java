package com.pippin.design.creational.singleton;

/**
 * 单例模式
 * 通过枚举类实现单例类，天然的序列化机制以及构造器不会影响单例的全局唯一性，且线程安全。
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public enum EnumInstance {
    INSTANCE {
        @Override
        protected void printTest() {
            System.out.println("pippin Print Test");
        }
    };

    protected abstract void printTest();

    private Object data;

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public static EnumInstance getInstance() {
        return INSTANCE;
    }

}
