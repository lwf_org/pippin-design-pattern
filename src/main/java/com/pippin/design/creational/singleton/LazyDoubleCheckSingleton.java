package com.pippin.design.creational.singleton;

/**
 * 单例模式（双重校验）
 * 两次判断非null，在两次判断中间使用synchronized，锁住类，实例全局只产生一个实例类，线程安全。
 * 因为通过反射可以调用构造器生成新的实例对象，所以在私有构造器中判断非null抛出异常。
 * 但如果先执行了反射，再通过getInstance（）方法生成实例类是可以通过，并且生成两个实例类。这个目前没办法解决。
 * 在同步代码块中，实例类指向lazyDoubleCheckSingleton也是有步骤顺序，如果不使用volatile修饰，jvm会指令重排序。
 * 在这期间可能会导致lazyDoubleCheckSingleton判断为null，导致又一线程进来生成实例类。
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class LazyDoubleCheckSingleton {
    private volatile static LazyDoubleCheckSingleton lazyDoubleCheckSingleton = null;

    private LazyDoubleCheckSingleton() {
        if (lazyDoubleCheckSingleton != null) {
            throw new RuntimeException("单例构造器禁止反射调用");
        }
    }

    public static LazyDoubleCheckSingleton getInstance() {
        if (lazyDoubleCheckSingleton == null) {
            synchronized (LazyDoubleCheckSingleton.class) {
                if (lazyDoubleCheckSingleton == null) {
                    lazyDoubleCheckSingleton = new LazyDoubleCheckSingleton();
                    //1.分配内存给这个对象
//                  //3.设置lazyDoubleCheckSingleton 指向刚分配的内存地址
                    //2.初始化对象
//                    intra-thread semantics
//                    ---------------//3.设置lazyDoubleCheckSingleton 指向刚分配的内存地址
                }
            }
        }
        return lazyDoubleCheckSingleton;
    }
}
