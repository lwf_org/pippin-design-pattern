package com.pippin.design.creational.singleton;

import java.io.Serializable;

/**
 * 单例模式（饿汉式）
 * 通过静态代码块以及静态对象，在类初始化时实例化单例类。
 * 因为通过反射可以调用构造器生成新的实例对象，所以在私有构造器中判断非null抛出异常。
 * 但通过反射、序列化、以及克隆等操作都可能会造成实例类不是全局唯一的。
 * 实现序列化接口，并新增readResolve()方法，返回实例对象，则序列化不会影响实例类全局唯一。
 * 实现克隆接口，重写clone（）方法，返回实例对象，则序列化不会影响实例类全局唯一。
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class HungrySingleton implements Serializable, Cloneable {

    private final static HungrySingleton HUNGRY_SINGLETON;

    static{
        HUNGRY_SINGLETON = new HungrySingleton();
    }
    private HungrySingleton(){
        if(HUNGRY_SINGLETON != null){
            throw new RuntimeException("单例构造器禁止反射调用");
        }
    }
    public static HungrySingleton getInstance(){
        return HUNGRY_SINGLETON;
    }

    /**
     * 名字必须为这个，序列化中有做判断
     * @return Object
     */
    private Object readResolve(){
        return HUNGRY_SINGLETON;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return getInstance();
    }
}
