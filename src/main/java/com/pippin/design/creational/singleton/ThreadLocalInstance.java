package com.pippin.design.creational.singleton;

/**
 * 单例模式（基于ThreadLocal实现线程唯一的单例实例）
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class ThreadLocalInstance {
    private static final ThreadLocal<ThreadLocalInstance> THREAD_LOCAL_INSTANCE_THREAD_LOCAL
            = ThreadLocal.withInitial(ThreadLocalInstance::new);

    private ThreadLocalInstance() {

    }

    public static ThreadLocalInstance getInstance() {
        return THREAD_LOCAL_INSTANCE_THREAD_LOCAL.get();
    }

}
