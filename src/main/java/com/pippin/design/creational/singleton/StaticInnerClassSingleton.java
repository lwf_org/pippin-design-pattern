package com.pippin.design.creational.singleton;

/**
 * 单例模式（内部类）
 * 静态内部类单例模式，使用静态内部类在被调用时才加载，实例懒加载，线程安全的。
 * 因为通过反射可以调用构造器生成新的实例对象，所以在私有构造器中判断非null抛出异常
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class StaticInnerClassSingleton {
    private static class InnerClass {
        private static StaticInnerClassSingleton staticInnerClassSingleton = new StaticInnerClassSingleton();
    }

    public static StaticInnerClassSingleton getInstance() {
        return InnerClass.staticInnerClassSingleton;
    }

    private StaticInnerClassSingleton() {
        if (InnerClass.staticInnerClassSingleton != null) {
            throw new RuntimeException("单例构造器禁止反射调用");
        }
    }


}
