package com.pippin.design.creational.simplefactory;

/**
 * 视频工厂类
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class VideoFactory {

    public AbstractVideo getVideo(String type) {
        if ("java".equalsIgnoreCase(type)) {
            return new JavaVideo();
        } else if ("python".equalsIgnoreCase(type)) {
            return new PythonVideo();
        }
        return null;
    }

    /**
     * 通过反射生成类
     *
     * @param c 类
     * @return AbstractVideo
     */
    public AbstractVideo getVideo(Class c) {
        AbstractVideo video = null;
        try {
            video = (AbstractVideo) Class.forName(c.getName()).newInstance();
        } catch (InstantiationException | ClassNotFoundException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return video;
    }

}
