package com.pippin.design.creational.simplefactory;

/**
 * 简单工厂测试类
 * 定义：由一个工厂对象决定创建出哪一种产品类的实例
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class Test {
    public static void main(String[] args) {
//        VideoFactory videoFactory = new VideoFactory();
//        Video video = videoFactory.getVideo("java");
//        if(video == null){
//            return;
//        }
//        video.produce();

        VideoFactory videoFactory = new VideoFactory();
        AbstractVideo video = videoFactory.getVideo(JavaVideo.class);
        if (video == null) {
            return;
        }
        video.produce();


    }

}
