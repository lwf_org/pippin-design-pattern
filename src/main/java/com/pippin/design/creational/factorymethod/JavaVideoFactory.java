package com.pippin.design.creational.factorymethod;

/**
 * 工厂实现类
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class JavaVideoFactory extends AbstractVideoFactory {

    @Override
    public AbstractVideo getVideo() {
        return new JavaVideo();
    }
}
