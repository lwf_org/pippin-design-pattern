package com.pippin.design.creational.factorymethod;

/**
 * 视频实现类
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class GoVideo extends AbstractVideo {

    @Override
    public void produce() {
        System.out.println("录制Go课程视频");
    }
}
