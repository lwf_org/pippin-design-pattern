package com.pippin.design.creational.factorymethod;

/**
 * 工厂模式
 * 定义：定义一个创建对象的接口，但让实现这个接口的类来决定实例化哪个类。工厂方法让类的实例化推迟到子类中进行。
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class Test {
    public static void main(String[] args) {
        AbstractVideoFactory videoFactory = new PythonVideoFactory();
        AbstractVideoFactory videoFactory2 = new JavaVideoFactory();
        AbstractVideoFactory videoFactory3 = new GoVideoFactory();
        AbstractVideo video = videoFactory.getVideo();
        video.produce();

    }

}
