package com.pippin.design.creational.factorymethod;

/**
 * 视频实现类
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class PythonVideo extends AbstractVideo {

    @Override
    public void produce() {
        System.out.println("录制Python课程视频");
    }
}
