package com.pippin.design.creational.factorymethod;

/**
 * 视频工厂抽象类
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public abstract class AbstractVideoFactory {

    /**
     * 父类定义，子类实现。返回视频实现类
     * @return AbstractVideo
     */
    public abstract AbstractVideo getVideo();

}
