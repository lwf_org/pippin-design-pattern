package com.pippin.design.creational.builder;

/**
 * 建造者模式课程抽象类
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public abstract class AbstractCourseBuilder {

    /**
     * 构建课程名
     *
     * @param courseName 课程名
     */
    public abstract void buildCourseName(String courseName);

    /**
     * 构建课程ppt
     *
     * @param coursePPT 课程ppt
     */
    public abstract void buildCoursePPT(String coursePPT);

    /**
     * 构建课程视频
     *
     * @param courseVideo 课程视频
     */
    public abstract void buildCourseVideo(String courseVideo);

    /**
     * 构建课程课件
     *
     * @param courseArticle 课程课件
     */
    public abstract void buildCourseArticle(String courseArticle);

    /**
     * 构建课程精简问答
     *
     * @param courseQA 课程精简问答
     */
    public abstract void buildCourseQA(String courseQA);

    /**
     * 构建课程
     *
     * @return Course
     */
    public abstract Course makeCourse();

}
