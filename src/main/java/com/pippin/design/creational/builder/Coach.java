package com.pippin.design.creational.builder;

/**
 * 建造者模式构建类
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class Coach {

    private AbstractCourseBuilder courseBuilder;

    public void setCourseBuilder(AbstractCourseBuilder courseBuilder) {
        this.courseBuilder = courseBuilder;
    }

    public Course makeCourse(String courseName,String coursePPT,
                             String courseVideo,String courseArticle,
                             String courseQA){
        this.courseBuilder.buildCourseName(courseName);
        this.courseBuilder.buildCoursePPT(coursePPT);
        this.courseBuilder.buildCourseVideo(courseVideo);
        this.courseBuilder.buildCourseArticle(courseArticle);
        this.courseBuilder.buildCourseQA(courseQA);
        return this.courseBuilder.makeCourse();
    }
}
