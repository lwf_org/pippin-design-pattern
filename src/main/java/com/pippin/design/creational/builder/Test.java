package com.pippin.design.creational.builder;

/**
 * 建造者模式
 * 定义：将一个复杂对象的构建与它的表示分离，使得同样的构建过程可以创建不同的表示。
 * 用户只需指定需要建造的类型就可以得到它们，建造过程及细节不需要知道。
 * 其实这里设计的有复杂
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class Test {
    public static void main(String[] args) {

        //实例化构建者，可以有不同的构建者，不同的构建方法
        AbstractCourseBuilder courseBuilder = new CourseActualBuilder();
        //实例构建类
        Coach coach = new Coach();
        //注入构建者
        coach.setCourseBuilder(courseBuilder);
        //调用构建类的构建者构建课程
        Course course = coach.makeCourse("Java设计模式精讲",
                "Java设计模式精讲PPT",
                "Java设计模式精讲视频",
                "Java设计模式精讲手记",
                "Java设计模式精讲问答");
        System.out.println(course);

    }
}
