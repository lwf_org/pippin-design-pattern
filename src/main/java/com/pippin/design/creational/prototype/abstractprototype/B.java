package com.pippin.design.creational.prototype.abstractprototype;

/**
 * 原型模式
 * 定义：指原型实例指定创建对象的种类，并通过拷贝这些原型创建新的对象
 * 不需要知道任何创建的细节，不调用构造函数。
 * 调用clone（）方法时，会将对象的属性值也克隆。
 * 但只会对基本数据类型做值传递，而引用数据类型则是引用传递。例如Date类型。
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class B extends A {
    public static void main(String[] args) throws CloneNotSupportedException {
        B b = new B();
        b.clone();
    }
}
