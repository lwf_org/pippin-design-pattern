package com.pippin.design.creational.prototype.abstractprototype;

/**
 * 抽象方法中也可以实现Cloneable接口，则其子类也同样实现了Cloneable接口。
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public abstract class A implements Cloneable{
    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
