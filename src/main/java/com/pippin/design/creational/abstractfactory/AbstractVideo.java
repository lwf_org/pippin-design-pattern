package com.pippin.design.creational.abstractfactory;

/**
 * 抽象工厂模式视频抽象类
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public abstract class AbstractVideo {

    /**
     * 抽象方法，父类定义，子类实现
     */
    public abstract void produce();
}
