package com.pippin.design.creational.abstractfactory;

/**
 * 抽象工厂模式文章实现类
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class PythonArticle extends AbstractArticle {
    @Override
    public void produce() {
        System.out.println("编写Python课程手记");
    }
}
