package com.pippin.design.creational.abstractfactory;

/**
 * 抽象工厂模式视频实现类
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class PythonVideo extends AbstractVideo {
    @Override
    public void produce() {
        System.out.println("录制Python课程视频");
    }
}
