package com.pippin.design.creational.abstractfactory;


/**
 * 抽象工厂模式课程工厂接口，也可用抽象类
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public interface CourseFactory {
    AbstractVideo getVideo();
    AbstractArticle getArticle();

}
