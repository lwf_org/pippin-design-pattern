package com.pippin.design.creational.abstractfactory;

/**
 * 抽象工厂模式
 * 定义：提供一个创建<p color="red">一系列</p>相关或互相依赖对象的接口。无需指定他们具体的类。
 * 就是工厂模式继续抽象，突出一系列。
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class Test {
    public static void main(String[] args) {
        CourseFactory courseFactory = new JavaCourseFactory();
        AbstractVideo video = courseFactory.getVideo();
        AbstractArticle article = courseFactory.getArticle();
        video.produce();
        article.produce();

        CourseFactory pythonCourseFactory = new PythonCourseFactory();
        AbstractVideo video1 = pythonCourseFactory.getVideo();
        AbstractArticle article1 = pythonCourseFactory.getArticle();
        video1.produce();
        article1.produce();
    }
}
