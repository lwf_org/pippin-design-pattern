package com.pippin.design.creational.abstractfactory;

/**
 * 抽象工厂模式课程实现类
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class PythonCourseFactory implements CourseFactory {
    @Override
    public AbstractVideo getVideo() {
        return new PythonVideo();
    }

    @Override
    public AbstractArticle getArticle() {
        return new PythonArticle();
    }
}
