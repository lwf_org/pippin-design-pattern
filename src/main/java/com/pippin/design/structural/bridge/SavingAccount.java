package com.pippin.design.structural.bridge;

/**
 * 账户接口实现类
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class SavingAccount implements Account {
    @Override
    public Account openAccount() {
        System.out.println("打开活期账号");
        //...
        return new SavingAccount();
    }

    @Override
    public void showAccountType() {
        System.out.println("这是一个活期账号");
    }
}
