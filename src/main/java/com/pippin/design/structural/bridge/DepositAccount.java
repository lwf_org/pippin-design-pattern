package com.pippin.design.structural.bridge;

/**
 * 账户接口实现类
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class DepositAccount implements Account {
    @Override
    public Account openAccount() {
        System.out.println("打开定期账号");
        return new DepositAccount();
    }

    @Override
    public void showAccountType() {
        System.out.println("这是一个定期账号");
    }
}
