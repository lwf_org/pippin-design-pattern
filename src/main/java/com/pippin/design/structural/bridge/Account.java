package com.pippin.design.structural.bridge;

/**
 * 账户接口
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public interface Account {

    // 开启账户接口
    Account openAccount();

    //账户类型
    void showAccountType();

}
