package com.pippin.design.structural.bridge;

/**
 * 银行实现类
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class ICBCBank extends Bank {
    public ICBCBank(Account account) {
        super(account);
    }

    @Override
    Account openAccount() {
        System.out.println("打开中国工商银行账号");
        account.openAccount();
        return account;
    }
}
