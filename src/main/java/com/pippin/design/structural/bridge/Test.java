package com.pippin.design.structural.bridge;

/**
 * 桥接模式
 * 定义：将抽象部分与它的具体实现部分分离，是它们都可以独立地变化。
 * 通过组合的方式简历两个类之间联系。
 *  一个类存在两个独立变化的维度，且这两个维度都需要进行扩展。
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class Test {
    public static void main(String[] args) {
        Bank icbcBank = new ICBCBank(new DepositAccount());
        Account icbcAccount = icbcBank.openAccount();
        icbcAccount.showAccountType();

        Bank icbcBank2 = new ICBCBank(new SavingAccount());
        Account icbcAccount2 = icbcBank2.openAccount();
        icbcAccount2.showAccountType();

        Bank abcBank = new ABCBank(new SavingAccount());
        Account abcAccount = abcBank.openAccount();
        abcAccount.showAccountType();
    }
}
