package com.pippin.design.structural.bridge;

/**
 * 银行实现类
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class ABCBank extends Bank {
    public ABCBank(Account account) {
        super(account);
    }

    @Override
    Account openAccount() {
        System.out.println("打开中国农业银行账号");
        account.openAccount();
        return account;
    }
}
