package com.pippin.design.structural.bridge;

/**
 * 银行抽象类
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public abstract class Bank {

    protected Account account;

    //在构造银行时就将账户类型传入
    public Bank(Account account){
        this.account = account;
    }

    abstract Account openAccount();

}
