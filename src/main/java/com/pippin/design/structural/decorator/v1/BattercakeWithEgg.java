package com.pippin.design.structural.decorator.v1;

/**
 * 装饰物（加个鸡蛋的煎饼类）
 * 其继承了煎饼类，但通过继承不好灵活扩展
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class BattercakeWithEgg extends Battercake {
    @Override
    public String getDesc() {
        return super.getDesc()+" 加一个鸡蛋";
    }

    @Override
    public int cost() {
        return super.cost()+1;
    }
}
