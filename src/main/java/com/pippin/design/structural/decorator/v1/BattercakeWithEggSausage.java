package com.pippin.design.structural.decorator.v1;

/**
 * 装饰物（加个鸡蛋和香肠的煎饼类）
 * 其继承了煎饼鸡蛋类，但通过继承不好灵活扩展
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class BattercakeWithEggSausage extends BattercakeWithEgg {
    @Override
    public String getDesc() {
        return super.getDesc()+ " 加一根香肠";
    }

    @Override
    public int cost() {
        return super.cost()+2;
    }
}
