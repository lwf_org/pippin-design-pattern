package com.pippin.design.structural.decorator.v1;

/**
 * 装饰者主体
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class Battercake {
    protected String getDesc(){
        return "煎饼";
    }
    protected int cost(){
        return 8;
    }

}
