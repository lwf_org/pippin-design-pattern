package com.pippin.design.structural.decorator.v2;

/**
 * 装饰主体抽象类
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public abstract class ABattercake {
    protected abstract String getDesc();
    protected abstract int cost();

}
