package com.pippin.design.structural.decorator.v2;

/**
 * 装饰主体
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class Battercake extends ABattercake {
    @Override
    protected String getDesc() {
        return "煎饼";
    }

    @Override
    protected int cost() {
        return 8;
    }
}
