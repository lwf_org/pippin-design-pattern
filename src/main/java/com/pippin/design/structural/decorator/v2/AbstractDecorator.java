package com.pippin.design.structural.decorator.v2;

/**
 * 装饰物抽象类
 * 其继承了装饰物主体抽象类，并且组合了装饰物主体抽象类
 * 这里和适配器模式的适配接口有点相似，继承或实现父类抽象类或者接口并组合。
 * 但适配器是将调用被适配类的方法适配成调用适配类中的方法，而装饰者是将装饰主体中添加修饰。
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public abstract class AbstractDecorator extends ABattercake {

    private ABattercake aBattercake;

    public AbstractDecorator(ABattercake aBattercake) {
        this.aBattercake = aBattercake;
    }

    protected abstract void doSomething();

    @Override
    protected String getDesc() {
        return this.aBattercake.getDesc();
    }

    @Override
    protected int cost() {
        return this.aBattercake.cost();
    }
}
