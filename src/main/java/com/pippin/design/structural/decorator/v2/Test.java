package com.pippin.design.structural.decorator.v2;

/**
 * 装饰者模式
 * 定义：将对象组合成树形接口以表示“整体-部分”的层次结构
 * 组合模式使客户端对单个对象和组合对象保持一致的方式处理
 *
 * 注意事项：可代替继承。
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 *
 */
public class Test {
    public static void main(String[] args) {
        ABattercake aBattercake;
        aBattercake = new Battercake();
        aBattercake = new EggDecorator(aBattercake);
        aBattercake = new EggDecorator(aBattercake);
        aBattercake = new SausageDecorator(aBattercake);

        System.out.println(aBattercake.getDesc() + " 销售价格:" + aBattercake.cost());

    }
}
