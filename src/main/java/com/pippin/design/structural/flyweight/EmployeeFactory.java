package com.pippin.design.structural.flyweight;

import java.util.HashMap;
import java.util.Map;

/**
 * 享元模式核心就是使用集合类存储经常使用相似的对象。
 * 分外部状态和内部状态：内部状态是将不变的属性做共享，外部状态则是由外部传入的数据。
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class EmployeeFactory {
    private static final Map<String,Employee> EMPLOYEE_MAP = new HashMap<>();

    public static Employee getManager(String department){
        Manager manager = (Manager) EMPLOYEE_MAP.get(department);

        if(manager == null){
            manager = new Manager(department);
            System.out.print("创建部门经理:"+department);
            String reportContent = department+"部门汇报:此次报告的主要内容是......";
            manager.setReportContent(reportContent);
            System.out.println(" 创建报告:"+reportContent);
            EMPLOYEE_MAP.put(department,manager);

        }
        return manager;
    }

}
