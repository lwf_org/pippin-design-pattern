package com.pippin.design.structural.flyweight;

/**
 * 享元模式接口
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public interface Employee {
    void report();
}
