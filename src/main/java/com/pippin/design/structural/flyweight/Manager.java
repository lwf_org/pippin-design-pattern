package com.pippin.design.structural.flyweight;

/**
 * 享元模式实现类
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class Manager implements Employee {
    @Override
    public void report() {
        System.out.println(reportContent);
    }
    private String title = "部门经理";
    private String department;
    private String reportContent;

    public void setReportContent(String reportContent) {
        this.reportContent = reportContent;
    }

    public Manager(String department) {
        this.department = department;
    }


}
