package com.pippin.design.structural.flyweight;

/**
 * 享元模式
 * 定义：提供了减少对象数量从而改善应用所需的对象结构方式。
 * 运用共享技术有效的支持大量细粒度的对象。
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class Test {
    private static final String departments[] = {"RD","QA","PM","BD"};

    public static void main(String[] args) {
        for(int i=0; i<10; i++){
            String department = departments[(int)(Math.random() * departments.length)];
            Manager manager = (Manager) EmployeeFactory.getManager(department);
            manager.report();

        }
        // 在jdk中Integer和Long类中有使用共享模式，默认共享-128 -- 127 的数值有做共享
//        Integer a = Integer.valueOf(100);
//        Integer b = 100;
//
//        Integer c = Integer.valueOf(1000);
//        Integer d = 1000;
//
//        System.out.println("a==b:"+(a==b));
//
//        System.out.println("c==d:"+(c==d));

    }
}
