package com.pippin.design.structural.facade;

/**
 * 物品
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class PointsGift {
    private String name;

    public PointsGift(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
