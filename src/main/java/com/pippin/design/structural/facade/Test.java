package com.pippin.design.structural.facade;

/**
 * 外观模式
 * 定义：又叫门面模式，提供了一个统一的接口，用来访问子系统中的一群接口
 * 外观模式定义了一个高层接口，让子系统更容易使用
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class Test {
    public static void main(String[] args) {
        PointsGift pointsGift = new PointsGift("T恤");
        GiftExchangeService giftExchangeService = new GiftExchangeService();
        giftExchangeService.giftExchange(pointsGift);
    }
}
