package com.pippin.design.structural.facade;

/**
 * 校验库存
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class QualifyService {
    public boolean isAvailable(PointsGift pointsGift){
        System.out.println("校验"+pointsGift.getName()+" 积分资格通过,库存通过");
        return true;
    }
}
