package com.pippin.design.structural.facade;

/**
 * 支付
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class PointsPaymentService {
    public boolean pay(PointsGift pointsGift){
        //扣减积分
        System.out.println("支付"+pointsGift.getName()+" 积分成功");
        return true;
    }

}
