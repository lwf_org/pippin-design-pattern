package com.pippin.design.structural.facade;

/**
 * 外观模式，将复杂的操作归总到一个地方对外提供操作。
 * 自我感觉这种方式有点类似于在控制层与业务层中再加一层biz业务层的意思。针对业务复杂的方法单独抽出来。
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class GiftExchangeService {
    private QualifyService qualifyService = new QualifyService();
    private PointsPaymentService pointsPaymentService = new PointsPaymentService();
    private ShippingService shippingService = new ShippingService();

    public void giftExchange(PointsGift pointsGift){
        if(qualifyService.isAvailable(pointsGift)){
            //资格校验通过
            if(pointsPaymentService.pay(pointsGift)){
                //如果支付积分成功
                String shippingOrderNo = shippingService.shipGift(pointsGift);
                System.out.println("物流系统下单成功,订单号是:"+shippingOrderNo);
            }
        }
    }

}
