package com.pippin.design.structural.proxy.dynamicproxy;


import com.pippin.design.structural.proxy.IOrderService;
import com.pippin.design.structural.proxy.Order;
import com.pippin.design.structural.proxy.OrderServiceImpl;

/**
 * (动态)代理模式
 * 定义：为其他对象提供一种代理，以控制对这个对象的访问。
 * 代理对象在客户端和目标对象之间起到中介的作用。
 *
 *  1、和适配器模式的区别：适配器模式主要改变所考虑对象的接口，而代理模式不能改变所代理类的接口。
 *  2、和装饰器模式的区别：装饰器模式为了增强功能，而代理模式是为了加以控制。
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class Test {
    public static void main(String[] args) {
        Order order = new Order();
//        order.setUserId(2);
        order.setUserId(1);
        IOrderService orderServiceDynamicProxy = (IOrderService) new OrderServiceDynamicProxy(new OrderServiceImpl()).bind();

        orderServiceDynamicProxy.saveOrder(order);
    }
}
