package com.pippin.design.structural.proxy;

/**
 * 数据层
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public interface IOrderDao {
    int insert(Order order);

}
