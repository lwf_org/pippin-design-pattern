package com.pippin.design.structural.proxy;

/**
 * 业务层
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public interface IOrderService {
    int saveOrder(Order order);
}
