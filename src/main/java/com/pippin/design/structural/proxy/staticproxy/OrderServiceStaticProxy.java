package com.pippin.design.structural.proxy.staticproxy;


import com.pippin.design.structural.proxy.IOrderService;
import com.pippin.design.structural.proxy.Order;
import com.pippin.design.structural.proxy.OrderServiceImpl;
import com.pippin.design.structural.proxy.db.DataSourceContextHolder;

/**
 *（静态）代理模式
 * 这里和装饰者模式和适配器模式有点类似。
 * 装饰者模式是用装饰物去装饰主体，而静态代理是将对象的控制权移交给动态代理对象。
 * 适配器模式是将控制权移交到了适配类中，但适配器模式强调的是对结果的适配，对于返回结果做不同的操作以达成适配。
 * 而静态代理强调不对原实现类的做修改，只是在其前、后或者调用期间中添加方法执行操作。
 * 比如动态切换数据源，实现类该操作的还是那样操作，但数据源切换了。
 * 1、和适配器模式的区别：适配器模式主要改变所考虑对象的接口，而代理模式不能改变所代理类的接口。
 * 2、和装饰器模式的区别：装饰器模式为了增强功能，而代理模式是为了加以控制。
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class OrderServiceStaticProxy {
    private IOrderService iOrderService;

    public int saveOrder(Order order){
        // 前置方法
        beforeMethod(order);
        // 因为没有使用注解注册bean，所以这里我们就直接new出来
        iOrderService = new OrderServiceImpl();
        int result = iOrderService.saveOrder(order);
        // 后置方法
        afterMethod();
        return result;
    }

    private void beforeMethod(Order order){
        int userId = order.getUserId();
        int dbRouter = userId % 2;
        System.out.println("静态代理分配到【db"+dbRouter+"】处理数据");

        // 设置dataSource。
        //我们这里也实现了动态切换数据源的操作
        DataSourceContextHolder.setDBType("db"+String.valueOf(dbRouter));
        System.out.println("静态代理 before code");
    }

    private void afterMethod(){
        System.out.println("静态代理 after code");
    }
}
