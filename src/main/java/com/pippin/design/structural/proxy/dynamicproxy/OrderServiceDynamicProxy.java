package com.pippin.design.structural.proxy.dynamicproxy;

import com.pippin.design.structural.proxy.Order;
import com.pippin.design.structural.proxy.db.DataSourceContextHolder;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * （动态）代理模式
 * 通过jdk的InvocationHandler接口重写invoke实现动态代理。
 * 是能代理接口实现类，无法代理没有实现接口的类做带。
 * 是由程序调用到动态代理对象时由jvm真正创建，jvm根据传进来的业务实现类对象以及方法名动态创建了一个代理类class文件。
 * 并将这个class文件被字节码引擎执行，然后通过该代理类的对象进行方法调用。
 *
 * cglib是可以代理类的，其就是针对类实现做代理的。其操作是继承被代理类生成一个子类，重写其方法。
 * 如果类是final或者方法时final的就不能被代理。
 *
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class OrderServiceDynamicProxy implements InvocationHandler {

    private Object target;

    public OrderServiceDynamicProxy(Object target) {
        this.target = target;
    }

    public Object bind(){
        Class cls = target.getClass();
        return Proxy.newProxyInstance(cls.getClassLoader(),cls.getInterfaces(),this);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object argObject = args[0];
        beforeMethod(argObject);
        Object object = method.invoke(target,args);
        afterMethod();
        return object;
    }

    private void beforeMethod(Object obj){
        int userId = 0;
        System.out.println("动态代理 before code");
        if(obj instanceof Order){
            Order order = (Order)obj;
            userId = order.getUserId();
        }
        int dbRouter = userId % 2;
        System.out.println("动态代理分配到【db"+dbRouter+"】处理数据");

        // 设置dataSource;
        DataSourceContextHolder.setDBType("db"+String.valueOf(dbRouter));
    }

    private void afterMethod(){
        System.out.println("动态代理 after code");
    }
}
