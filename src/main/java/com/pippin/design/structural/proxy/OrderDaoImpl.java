package com.pippin.design.structural.proxy;

/**
 * 数据层
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class OrderDaoImpl implements IOrderDao {
    @Override
    public int insert(Order order) {
        System.out.println("Dao层添加Order成功");
        return 1;
    }
}
