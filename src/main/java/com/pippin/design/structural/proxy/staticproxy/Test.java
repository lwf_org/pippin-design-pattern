package com.pippin.design.structural.proxy.staticproxy;


import com.pippin.design.structural.proxy.Order;

/**
 *（静态）代理模式
 * 定义：为其他对象提供一种代理，以控制对这个对象的访问。
 * 代理对象在客户端和目标对象之间起到中介的作用。
 *
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class Test {
    public static void main(String[] args) {
        Order order = new Order();
        order.setUserId(2);

        OrderServiceStaticProxy orderServiceStaticProxy = new OrderServiceStaticProxy();
        orderServiceStaticProxy.saveOrder(order);
    }
}
