package com.pippin.design.structural.adapter;

/**
 * （对象）适配器模式
 * 实现适配接口，进行适配。
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class PowerAdapter implements DC5{
    private AC220 ac220 = new AC220();

    @Override
    public int outputDC5V() {
        int adapterInput = ac220.outputAC220V();
        //变压器...
        int adapterOutput = adapterInput/44;

        System.out.println("使用PowerAdapter输入AC:"+adapterInput+"V"+"输出DC:"+adapterOutput+"V");
        return adapterOutput;
    }
}
