package com.pippin.design.structural.adapter.objectadapter;

/**
 * 适配器模式
 * 期望实现的接口
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public interface Target {
    void request();
}
