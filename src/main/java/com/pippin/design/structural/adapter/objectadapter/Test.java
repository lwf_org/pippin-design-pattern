package com.pippin.design.structural.adapter.objectadapter;

/**
 * （对象）适配器模式
 * 通过对象适配模式，我们这里将本来需要操作Adaptee类中的方法工作移交到了Adapter类中，在Adapter可以修改方法。就是做适配。
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class Test {
    public static void main(String[] args) {
        Target adapterTarget = new Adapter();
        adapterTarget.request();

    }
}
