package com.pippin.design.structural.adapter.classadapter;

/**
 * 适配器模式
 * 被适配的类
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class Adaptee {

    public void adapteeRequest(){
        System.out.println("被适配者的方法");
    }

}
