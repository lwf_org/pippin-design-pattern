package com.pippin.design.structural.adapter;

/**
 * 适配器模式
 * 期望实现的接口
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public interface DC5 {

    /**
     * 适配方法
     * @return int 适配后的电流值
     */
    int outputDC5V();
}
