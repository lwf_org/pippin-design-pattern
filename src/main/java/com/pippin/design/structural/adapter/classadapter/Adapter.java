package com.pippin.design.structural.adapter.classadapter;

/**
 * 适配器模式
 * 继承了被适配者，实现了目标接口。
 * 主要是通过<p color="red">继承</p>实现了调用被适配者的方法进行适配。
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class Adapter extends Adaptee implements Target{

    @Override
    public void request() {
        // 这里进行对被适配者的适配，就是修改操作
        super.adapteeRequest();
        //...
    }
}
