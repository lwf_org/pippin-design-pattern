package com.pippin.design.structural.adapter;

/**
 * 适配器模式
 * 定义：将一个类的接口转换成客户期望的另一个接口。
 * 使原本接口不兼容的类可以一起工作
 * 注意事项：适配器不是在详细设计时添加的，而是解决正在服役的项目的问题。
 *
 * @author : Natus
 * @date : 2020/8/27 0027
 */
public class Test {
    public static void main(String[] args) {
        DC5 dc5 = new PowerAdapter();
        dc5.outputDC5V();

    }
}
